FROM containers.ligo.org/toolbox/igwn-toolbox:el9

COPY extra-packages /
RUN dnf -y groupinstall "Development Tools" && \
    dnf -y groupinstall "RPM Development Tools" && \
    dnf -y install $(<extra-packages) && \
    rm /extra-packages && \
    dnf clean all

RUN mv /etc/koji.conf /etc/koji-fedora.conf
COPY --chmod=0644 --chown=root:mock /environment/htcondor.tpl /etc/mock/templates/htcondor.tpl
COPY --chmod=0644 --chown=root:mock /environment/igwn-backports-testing.tpl /etc/mock/templates/igwn-backports-testing.tpl
COPY --chmod=0644 --chown=root:mock /environment/igwn-backports.tpl /etc/mock/templates/igwn-backports.tpl
COPY --chmod=0644 --chown=root:mock /environment/igwn-epel-8.tpl /etc/mock/templates/igwn-epel-8.tpl
COPY --chmod=0644 --chown=root:mock /environment/igwn-production.tpl /etc/mock/templates/igwn-production.tpl
COPY --chmod=0644 --chown=root:mock /environment/igwn-rocky-8-x86_64.cfg /etc/mock/igwn-rocky-8-x86_64.cfg
COPY --chmod=0644 --chown=root:mock /environment/igwn-rocky-8.tpl /etc/mock/templates/igwn-rocky-8.tpl
COPY --chmod=0644 --chown=root:mock /environment/igwn-rocky-9-x86_64.cfg /etc/mock/igwn-rocky-9-x86_64.cfg
COPY --chmod=0644 --chown=root:mock /environment/igwn-rocky-epel-8-x86_64.cfg /etc/mock/igwn-rocky-epel-8-x86_64.cfg
COPY --chmod=0644 --chown=root:mock /environment/igwn-testing.tpl /etc/mock/templates/igwn-testing.tpl
COPY --chmod=0644 --chown=root:mock /environment/osg.tpl /etc/mock/templates/osg.tpl
COPY --chmod=0644 /environment/koji.conf /etc/koji.conf

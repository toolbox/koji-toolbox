config_opts['dnf.conf'] += """

[igwn-testing]
name=IGWN Testing Software (Rocky Linux $releasever) - $basearch
baseurl=https://software.igwn.org/lscsoft/rocky/$releasever/testing/$basearch/os/
enabled=1
gpgcheck=0
skip_if_unavailable=False

[igwn-testing-source]
name=IGWN Testing Software (Rocky Linux $releasever) - Source
baseurl=https://software.igwn.org/lscsoft/rocky/$releasever/testing/SRPMS/
enabled=0
gpgcheck=0
skip_if_unavailable=False

"""

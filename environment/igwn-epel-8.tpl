config_opts['chroot_setup_cmd'] += " epel-rpm-macros"

config_opts['dnf.conf'] += """

[igwn-epel]
name=IGWN EPEL Snapshot for Enterprise Linux $releasever - $basearch
baseurl=https://software.igwn.org/lscsoft/epel/$releasever/Everything/$basearch
gpgcheck=1
gpgkey=file:///usr/share/distribution-gpg-keys/epel/RPM-GPG-KEY-EPEL-$releasever
skip_if_unavailable=False

[igwn-epel-debuginfo]
name=IGWN EPEL Snapshot for Enterprise Linux $releasever - $basearch - Debug
baseurl=https://software.igwn.org/lscsoft/epel/$releasever/Everything/$basearch/debug/
enabled=0
gpgcheck=1
gpgkey=file:///usr/share/distribution-gpg-keys/epel/RPM-GPG-KEY-EPEL-$releasever
skip_if_unavailable=False

[igwn-epel-source]
name=IGWN EPEL Snapshot for Enterprise Linux $releasever - $basearch - Source
baseurl=https://software.igwn.org/lscsoft/epel/$releasever/Everything/source/tree/
enabled=0
gpgcheck=1
gpgkey=file:///usr/share/distribution-gpg-keys/epel/RPM-GPG-KEY-EPEL-$releasever
skip_if_unavailable=False

"""

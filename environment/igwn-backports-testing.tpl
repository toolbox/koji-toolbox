config_opts['dnf.conf'] += """

[igwn-backports-testing]
name=IGWN Backports Testing Software (Rocky Linux $releasever) - $basearch
baseurl=https://software.igwn.org/lscsoft/rocky/$releasever/backports-testing/$basearch/os/
enabled=1
gpgcheck=0
skip_if_unavailable=False

[igwn-backports-testing-source]
name=IGWN Backports Testing Software (Rocky Linux $releasever) - Source
baseurl=https://software.igwn.org/lscsoft/rocky/$releasever/backports-testing/SRPMS/
enabled=0
gpgcheck=0
skip_if_unavailable=False

"""

config_opts['dnf.conf'] += """

[igwn-backports]
name=IGWN Backports Software (Rocky Linux $releasever) - $basearch
baseurl=https://software.igwn.org/lscsoft/rocky/$releasever/backports/$basearch/os/
enabled=1
gpgcheck=0
skip_if_unavailable=False

[igwn-backports-source]
name=IGWN Backports Software (Rocky Linux $releasever) - Source
baseurl=https://software.igwn.org/lscsoft/rocky/$releasever/backports/SRPMS/
enabled=0
gpgcheck=0
skip_if_unavailable=False

"""

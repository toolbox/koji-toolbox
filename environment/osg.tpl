config_opts['dnf.conf'] += """

[osg]
name=OSG Software for Enterprise Linux $releasever - $basearch
#baseurl=https://repo.opensciencegrid.org/osg/24-main/el$releasever/release/$basearch
mirrorlist=https://repo.opensciencegrid.org/mirror/osg/24-main/el$releasever/release/$basearch
priority=98
enabled=1
gpgcheck=1
gpgkey=https://repo.opensciencegrid.org/osg/RPM-GPG-KEY-OSG-24-developer
skip_if_unavailable=true
exclude=*condor*

"""

config_opts['dnf.conf'] += """

[igwn-production]
name=IGWN Production Software (Rocky Linux $releasever) - $basearch
baseurl=https://software.igwn.org/lscsoft/rocky/$releasever/production/$basearch/os/
enabled=1
gpgcheck=0
skip_if_unavailable=False

[igwn-production-source]
name=IGWN Production Software (Rocky Linux $releasever) - Source
baseurl=https://software.igwn.org/lscsoft/rocky/$releasever/production/SRPMS/
enabled=0
gpgcheck=0
skip_if_unavailable=False

"""

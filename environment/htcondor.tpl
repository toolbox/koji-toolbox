config_opts['dnf.conf'] += """

[htcondor]
name=HTCondor for Enterprise Linux $releasever - Release
baseurl=https://research.cs.wisc.edu/htcondor/repo/24.x/el$releasever/$basearch/release
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://research.cs.wisc.edu/htcondor/repo/keys/HTCondor-24.x-Key
priority=90
skip_if_unavailable=true

"""
